public class Circle extends GeometricalShapes {
   public static double pi= 3.14;
   public int radius= 3;
   public static String name = "circle";

    @Override
    public double getSquare() {
        return (pi*radius*radius);
    }

    @Override
    public String whatIsFigure() {
        return name;
    }

    public Circle(int radius) {
        this.radius = radius;
    }
}
