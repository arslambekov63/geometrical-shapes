public abstract class GeometricalShapes {
    public abstract double getSquare();
    public abstract String whatIsFigure();
}
