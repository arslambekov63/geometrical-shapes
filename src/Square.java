public class Square extends GeometricalShapes {
    public int side;
    public static String name= "square";

    @Override
    public double getSquare() {
        return (side*side);
    }

    @Override
    public String whatIsFigure() {
        return (name);
    }

    public Square(int side) {
        this.side = side;
    }
}
