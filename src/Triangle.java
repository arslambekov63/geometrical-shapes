public class Triangle extends GeometricalShapes {
    public int base;
    public int height;
   public  static String name =  "triangle";
    @Override
    public double getSquare() {
        return (0.5*base*height);
    }

    @Override
    public String whatIsFigure() {
        return (name);
    }

    public Triangle(int base, int height) {
        this.base = base;
        this.height = height;
    }
}
